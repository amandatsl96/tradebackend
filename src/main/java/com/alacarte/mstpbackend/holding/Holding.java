package com.alacarte.mstpbackend.holding;

import com.alacarte.mstpbackend.trade.TradeState;
import com.alacarte.mstpbackend.trade.TradeType;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
@Data
public class Holding {

    @Id
    @Getter
    @Setter
    private String id;
    private String ticker;
    private int bought;
    private int sold;
    private String userId;

    public Holding() {}

    public Holding(String ticker, TradeType type, int quantity, String userId) {
        this.userId = userId;
        this.ticker = ticker;
        if (type == TradeType.BUY) {
            bought = quantity;
        }
    }

}
