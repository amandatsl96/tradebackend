package com.alacarte.mstpbackend.trade;

public enum TradeType {
    BUY("BUY"),
    SELL("SELL");

    private String tradeType;

    TradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getTradeType() {
        return this.tradeType;
    }
}
