package com.alacarte.mstpbackend.trade;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
@Data
public class Trade {

    @Id
    @Getter
    @Setter
    private String id;
    private Date created = new Date(System.currentTimeMillis());
    private TradeState state = TradeState.CREATED;
    private TradeType type;
    private String ticker;
    private int quantity;
    private double unitPrice;
    private double totalPrice;
    private String userId;

    //Constructors
    public Trade(){}

    public Trade(String ticker, TradeType type, int quantity, double unitPrice, String userId) {
        this.ticker = ticker;
        this.type = type;
        this.quantity = quantity;
        this.userId = userId;
        this.unitPrice = unitPrice;
        totalPrice = unitPrice * quantity;
    }
}
