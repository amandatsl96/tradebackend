package com.alacarte.mstpbackend.utils;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class DataService {

    @Autowired
    RestTemplate restTemplate;

    @Value("${data.service.uri}")
    private String uri;

    public Double fetchCurrentPrice(String ticker) {
        String fullUri = uri + "/real/" + ticker;
        ResponseEntity<JsonNode> response = restTemplate.getForEntity(fullUri, JsonNode.class);
        Double price = response.getBody().get("latestPrice").asDouble();
        return price;
    }

}
