package com.alacarte.mstpbackend;

import com.alacarte.mstpbackend.holding.Holding;
import com.alacarte.mstpbackend.holding.HoldingRepository;
import com.alacarte.mstpbackend.trade.Trade;
import com.alacarte.mstpbackend.trade.TradeRepository;
import com.alacarte.mstpbackend.trade.TradeType;
import com.alacarte.mstpbackend.utils.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TradeService {

    @Autowired
    private TradeRepository tradeRepository;

    @Autowired
    private HoldingRepository holdingRepository;

    @Autowired
    private DataService dataService;

    //Create Trade
    public Trade create(String ticker, TradeType type, int quantity, String userId) {
        //data service container linked! :)
        double unitPrice = dataService.fetchCurrentPrice(ticker);
        if (verifyTrade(ticker, type, quantity, userId)) {
            changeHolding(ticker, type, quantity, userId);
            return tradeRepository.save(new Trade(ticker, type, quantity, unitPrice, userId));
        } else {
            return new Trade();
        }
    }

    public boolean verifyTrade(String ticker, TradeType type, int quantity, String userId) {
        if (type == TradeType.BUY) return true;
        if (type == TradeType.SELL) {
            List<Holding> holdings = holdingRepository.findAllByUserIdAndTicker(userId, ticker);
            if (! holdings.isEmpty()) {
                Holding h = holdings.get(0);
                int balance = h.getBought() - h.getSold();
                if (balance >= quantity) return true;
            }
        }
        return false;
    }

    public void changeHolding(String ticker, TradeType type, int quantity, String userId) {
        List<Holding> holdings = holdingRepository.findAllByUserIdAndTicker(userId, ticker);
        if (holdings.isEmpty()) {
            Holding h = new Holding(ticker, type, quantity, userId);
            holdingRepository.save(h);
        } else {
            Holding h = holdings.get(0);
            if (type == TradeType.BUY) {
                h.setBought(h.getBought() + quantity);
            } else if (type == TradeType.SELL) {
                h.setSold(h.getSold() + quantity);
            }
            holdingRepository.save(h);
        }
    }

    //Retrieve All Trades
    public List<Trade> getAll() {return tradeRepository.findAll();}

    //Retrieve ALL Trades History by User
    //Trade history refers to the transactions the user has BUY/SELL
    public List<Trade> getHistory(String userId, String ticker, Date from, Date to) {
        if (ticker != null && from != null && to != null) {
            return tradeRepository.findAllByUserIdAndTickerAndCreatedBetween(userId, ticker, from, to);
        } else if (ticker == null && from != null && to != null) {
            return tradeRepository.findAllByUserIdAndCreatedBetween(userId, from, to);
        } else if (ticker != null) {
            return tradeRepository.findAllByUserIdAndTicker(userId, ticker);
        } else {
            return tradeRepository.findAllByUserId(userId);
        }
    }

    //The SUMMARY of the stocks the user possesses
    //Returns stock ticker and quantity
    public List<Holding> getHoldings(String userId, String ticker){
        if (ticker == null) {
            return holdingRepository.findAllByUserId(userId);
        } else {
            return holdingRepository.findAllByUserIdAndTicker(userId, ticker);
        }
    }

    //Return list of distinct tickers belonging to a user
    public List<String> getUserTickers(String userId){
        Set<String> tickers = new HashSet<>();
        List<Trade> trades = tradeRepository.findAllByUserId(userId);
        for (int i=0; i<trades.size(); i++){
            Trade t = trades.get(i);
            String name = t.getTicker();
            if (!tickers.contains(name)){
                tickers.add(name);
            }
        }
        List<String> tickersList = new ArrayList<>();
        tickersList.addAll(tickers);
        return tickersList;
    }

    /*
    //Calculate trade total cost
    public Double calculateTradeTotalPrice(String stockId, int quantity){
        Trade trade = tradeRepository.getTrade(stockId);
        Double totalPrice = trade.getPrice() * quantity;
        return totalPrice;
    }
     */
}
